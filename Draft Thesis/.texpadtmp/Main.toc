\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Literature Review}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Self-Regulation}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Indicators}{2}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Goal Orientation}{3}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Goal Tracking Status}{3}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Goal Completion Status}{3}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Time Management}{3}{subsubsection.2.2.4}
\contentsline {subsubsection}{\numberline {2.2.5}Quiz Mark}{3}{subsubsection.2.2.5}
\contentsline {subsubsection}{\numberline {2.2.6}Help Seeking}{3}{subsubsection.2.2.6}
\contentsline {subsubsection}{\numberline {2.2.7}Study Kit}{3}{subsubsection.2.2.7}
\contentsline {subsubsection}{\numberline {2.2.8}Final Grade}{3}{subsubsection.2.2.8}
\contentsline {subsection}{\numberline {2.3}Goal Tracking Example}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Work Statement}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}System Architecturing}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Architecture}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Package \& Dependencies}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Interface Design}{6}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Typical Use Cases}{7}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Use case 1}{7}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Use case 2}{8}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Use case 3}{8}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Use case 4}{8}{subsubsection.3.5.4}
\contentsline {subsection}{\numberline {3.6}Data Modelling}{9}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Code Structure}{10}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Requirements Specification}{11}{subsection.3.8}
\contentsline {subsubsection}{\numberline {3.8.1}Functional Requirements}{11}{subsubsection.3.8.1}
\contentsline {subsubsection}{\numberline {3.8.2}Non-functional Requirements}{11}{subsubsection.3.8.2}
\contentsline {subsection}{\numberline {3.9}Limitations}{11}{subsection.3.9}
\contentsline {subsubsection}{\numberline {3.9.1}Library Limitation}{11}{subsubsection.3.9.1}
\contentsline {subsubsection}{\numberline {3.9.2}Acception Test Limitation}{11}{subsubsection.3.9.2}
\contentsline {subsection}{\numberline {3.10}Risk Factors}{11}{subsection.3.10}
\contentsline {subsubsection}{\numberline {3.10.1}Technical Risk}{11}{subsubsection.3.10.1}
\contentsline {subsubsection}{\numberline {3.10.2}Scope Creep}{12}{subsubsection.3.10.2}
\contentsline {subsubsection}{\numberline {3.10.3}Late Delivery}{12}{subsubsection.3.10.3}
\contentsline {section}{\numberline {4}Plan for the next semester}{12}{section.4}
\contentsline {subsection}{\numberline {4.1}Front-end Implementation}{12}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Back-end Implementation}{12}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Deployment \& Acceptance Testing}{12}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Draft thesis}{12}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Thesis \& Presentation}{12}{subsection.4.5}
\contentsline {section}{\numberline {5}Reference}{12}{section.5}
