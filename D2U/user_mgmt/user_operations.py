#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import getopt
import json
import sys

import django
import django.contrib.auth
from django import forms
from django.core.exceptions import ValidationError, ObjectDoesNotExist

import user_mgmt.models


def find_or_add(request, subject_field_name=None):
    """
    :param request: Object encoding the HTML request
    :param subject_field_name: Field from where to extract the username (
    optional)

    Method to check if the received request corresponds to an already existing
    user. If so, it logs it in, if not, creates the use and logs it in. The
    login is done based in a user name taken from:

    request.META.REMOTE_USER
    
    Shibboleth user in the environment!
    
    Return boolean stating if the user could be logged in
    """

    username = None

    # Find the username
    if subject_field_name is not None:
        username = request.POST.get(subject_field_name)

    if username is None or username == '':
        username = request.environ.get('mail')
    if username is None or username == '':
        username = request.environ.get('email')
    if username is None or username == '':
        username = request.environ.get('HTTP_MAIL')
    if username is None or username == '':
        username = request.environ.get('REMOTE_USER')
    if username is None or username == '':
        username = request.META.get('REMOTE_USER')
    if username is None or username == '':
        username = request.environ.get('USER')

    # Username has not been found, give it an "anonymous"
    if username is None or username == '':
        username = "_anonymous_"

    # Look up the user, and if not present, activate the account.
    user = user_lookup_by_email(username)
    if user is None:
        user = activate_account(username)

    # Start session
    login(request, user)

    return True


def user_lookup_by_email(username):
    """
    :param username: Lookup a user. If none is found return NONE, otherwise,
    return the user object.
    """

    try:
        user_model = django.contrib.auth.get_user_model()
        user = user_model.objects.get(email__exact=username.lower())
    except ObjectDoesNotExist:
        return None

    return user


def user_lookup_by_id(user_id):
    """
    :param user_id: Lookup a user. If none is found return NONE, otherwise,
    return the user object.
    """

    try:
        user_model = django.contrib.auth.get_user_model()
        user = user_model.objects.get(id=user_id)
    except ObjectDoesNotExist:
        return None

    return user


def activate_account(user_id):
    """
    :param user_id: activate a user account for this user_id
    """
    
    user_model = django.contrib.auth.get_user_model()
    user = user_model.objects.create_user(user_id.lower())
    user.is_active = True
    user.save()
    return user


def activate_account_with_email(email):
    """
    :param email: Given the email value checks that it is a well formed
    email and creates the account.

    :return A string with the error message or the user Object
    """

    f = forms.EmailField()
    
    try:
        email = f.clean(email.strip().lower())
    except ValidationError:
        return '{0}: Enter valid email address'.format(email)

    user = user_lookup_by_email(email)
    if user is None:
        user = activate_account(email)
    else:
        return '{0} already exists. Skipping'.format(email)

    return user


def login(request, user):
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    django.contrib.auth.login(request, user)


def get_all_user_names():
    """
    Function to fetch all the users in a query set and return it.
    """
    user_model = django.contrib.auth.get_user_model()

    return sorted(user_model.objects.values_list('email', flat=True))


def find_or_add_user_context_record_by_email(user_email, context):
    """
    Given the email, either find the user,context record in the personal
    record table, or create an empty one.

    :param user_email: Email to identify the user
    :param context: context string to identify the record
    :return: The DB entry.

    """

    # Get the user object from the email, and call the auxiliary function
    user = user_lookup_by_email(user_email)
    return find_or_add_user_context_record(user, context)


def find_user_context_record_by_email(user_email, context):
    """
    Given the email, find the user,context record in the personal
    record table.

    :param user_email: Email to identify the user
    :param context: context string to identify the record
    :return: The DB entry.

    """

    # Get the user object from the email, and call the auxiliary function
    user = user_lookup_by_email(user_email)
    return find_user_context_record(user, context)


def find_user_context_record(user, context):
    """
    Find the user,context record in the personal record table.

    :param user: User object
    :param context: context string to identify the record
    :return: The DB entry.

    """

    try:
        user_record = user_mgmt.models.PersonalRecord.objects.get(
            user=user, context=context)
    except ObjectDoesNotExist:
        user_record = None

    # Return result
    return user_record


def find_or_add_user_context_record(user, context):
    """
    Either find the user,context record in the personal record table,
    or create an empty one.

    :param user: User object
    :param context: context string to identify the record
    :return: The DB entry.

    """

    user_record = find_user_context_record(user, context)
    if user_record is None:
        try:
            ctx = user_mgmt.models.Context.objects.get(name=context)
        except ObjectDoesNotExist as e:
            print('Context', context, 'not found. Aborting script.')
            sys.exit(1)

        user_record = user_mgmt.models.PersonalRecord(
            user=user,
            context=ctx,
            dictionary=json.dumps({}))
        user_record.save()

    # Return result
    return user_record


def create_or_update_user_context_record(user_email, context, add_dict):
    """

    :param user_email: Email to identify the user
    :param context:
    :param add_dict:
    :return:
    """
    # Find or add the element
    user_record = find_or_add_user_context_record_by_email(user_email, context)

    # Update its dictionary. First JSON -> Data, then update, then DATA -> JSON
    user_dict = json.loads(user_record.dictionary)
    user_dict.update(add_dict)
    user_record.dictionary = json.dumps(user_dict)

    # Save the updated record in the database
    user_record.save()


def main():
    """
    Script to execute several commands related to user management. 

    script [options] command param1 ... 

    It accepts the following options and parameter:

      - d: Turns on debug

      - h: Shows this message.

    The commands accepted are:

      - useradd <email address>: Adds the user given as parameter
      
      - bulkuseradd: Adds the users comming as single line values in stdin # is 
        taken as a comment if appears at the beginning of the line
    """

    # Variables controlled by the options
    debug = False

    try: 
        opts, args = getopt.getopt(sys.argv[1:],
                                   "dh")
    except getopt.GetoptError as e:
        print(e.msg)
        print(main.__doc__)
        sys.exit(2)

    # Process the arguments
    for optstr, value in opts:
        if optstr == "-d":
            debug = True
        elif optstr == "-h":
            print(main.__doc__)
            sys.exit(0)

    # Process the remaining arguments
    if len(args) < 1:
        print("The script needs at least one command.")
        sys.exit(2)

    command_value = args.pop(0)
    if debug:
        print('Detected command', command_value)

    # One operation per command
    if command_value == 'useradd':
        # There must be an email address
        if len(args) < 1:
            print('Useradd needs an email address')
            sys.exit(2)

        msg = activate_account_with_email(args.pop(0))

        # If msg is a string, something went wrong.
        if type(msg) is str:
            print(msg)
            sys.exit(2)

    elif command_value == 'bulkuseradd':
        # Loop over stdin and add the emails
        for line in sys.stdin:
            line = line[:-1].strip()

            # If starts with comment or empty, skip
            if line == '' or line[0] == '#':
                continue
            
            msg = activate_account_with_email(line)
            # If msg is a string, something went wrong.
            if type(msg) is str:
                print(msg)
            
    else:
        print('Command', command_value, 'not valid.')
        print(main.__doc__)
        sys.exit(1)


def get_personalrecord_value(username, record_name, default_value):
    """

    :param username: Username to search the DB
    :param record_name: name of the record to find
    :param default_value Value to return if the record is not found
    :return: Text value of the requested record_name or default_value
    """

    try:
        # Fetch the personal record with the given user name and with a
        # dictionary containing the given record_name
        precord = user_mgmt.models.PersonalRecord.objects.get(
            user__email=username,
            dictionary__contains='"' + record_name + '"')
        pdict = json.loads(precord.dictionary)
        value = pdict.get(record_name, default_value)
    except user_mgmt.models.PersonalRecord.DoesNotExist:
        value = default_value

    return value

if __name__ == "__main__":
    main()
