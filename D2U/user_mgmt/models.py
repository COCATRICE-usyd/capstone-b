#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from django.conf import settings
from django.contrib.auth.base_user import (
    AbstractBaseUser, BaseUserManager
)
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _

from accumulate import load_module


class D2UserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff,
                          is_active=True,
                          is_superuser=is_superuser,
                          last_login=now,
                          date_joined=now,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class D2User(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    username = models.EmailField(_('email address'), max_length=254)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=('Designates whether the user can log into this admin ' +
                   'site.'))
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=('Designates whether this user should be treated as ' +
                   'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = D2UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        :return: Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])


#
# Context for data collection and pointer to module with handler
#
class Context(models.Model):

    # Context identifying the set of variables
    name = models.CharField(max_length=256, primary_key=True, unique=True)

    # Absolute path to the Python module containing the handler
    handler_path = models.CharField(max_length=2048)

    # Field to the members of the context
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                     related_name='contexts')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Context')
        verbose_name_plural = _('Contexts')


#
# Generic user data. Basically a table with the user id, the usual suspects
# in terms of fields, and then a huge blob for a JSON dictionary. Far from
# optimal and very NOSQL-esque, but for the time being, it should do it.
#
class PersonalRecord(models.Model):

    # User pointer
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             blank=False,
                             null=False,
                             on_delete=models.CASCADE)

    # Object
    context = models.ForeignKey(Context,
                                blank=False,
                                null=False,
                                on_delete=models.CASCADE)

    # JSON Dictionary with all the personal data.
    dictionary = models.TextField()


def load_context_module(context_name):
    """Function that given a context name loads its module as specified in
    the database
    """

    context_object = Context.objects.get(name=context_name)

    if context_object is None or context_object.handler_path == '':
        raise Exception('No handler in user_mgmt.' +
                        'Context for name {0}'.format(
                            context_name))
    return load_module(context_object.handler_path), context_object
