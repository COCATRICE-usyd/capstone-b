/* Author: Abelardo Pardo (<abelardo.pardo@sydney.edu.au>) */
function resizeIframe(obj)
{
    obj.style.height = 0;
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

function exco_grade(element, prob_idx, sequence_id, rseed) {

    // Fetch the value of the button pressed from within the form
    var v = $(element).parent().parent().find("input:checked").attr("value");
    // Ignore if no button has been selected
    if (v == undefined) {
	    return;
    }
    data_sent = {
        'type': 'POST',
        'url': 'up_attempt',
        'data': {'sequence_id': sequence_id,
	             'problem_index': prob_idx,
	             'selected': v,
	             'rseed': rseed
	    },
    };

    // Send event to the server
    var response = '';
    $.ajax(data_sent).done(function (data) {
	    // data is the javascript object from the json response
        // Display the proper button correct/incorrect
        $(".exco_grade_button").css('display', 'none');
	    response = data['result'];
        if (response == "correct") {
	        $(".exco_grade_button_correct").css('display', 'inline');
        } else if (response == "incorrect") {
            $(".exco_grade_button_incorrect").css('display', 'inline');
        } else {
            $(".exco_grade_button_error").css('display', 'inline');
        }
    }).fail(function() {
        $(".exco_grade_button_error").css('display', 'inline');
    });
    $(".exco_answer_button").attr('disabled', true);
    return;
};

