#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function

import os
import sys
import getopt
import codecs
import re
import itertools
import csv
import json

#
# Load up the variable to use a concrete django instance
#
import accumulate

# os.environ["DJANGO_SETTINGS_MODULE"] = "data2u.settings"
# sys.path.append(os.path.expanduser(os.path.join('~', 'django')))

# Loads the models so that they are used within the script.
import django
# from django.db import models
# django.setup()

import user_mgmt.models
import django.contrib.auth.models
import django.forms.models
import data2u.settings
import accumulate.models
import user_mgmt

expr_mark_re = re.compile('^\.\. exp-eval:: (?P<exp_str>.*)$')
expr_mark_final_re = re.compile('^\.\. exp-final-eval:: (?P<exp_str>.*)$')

case_debug = False


def load_csv_file(filename):
    """Load a CSV file with the user email in the first column and a set of
    additional columns with data. The names of the columns is taken from the
    first row.

       Returns the dictionary (user_email, dict(key,value))
    """

    # Empty dictionary as result
    result = {}

    # Open the file and prepare to read as CSV
    data_in = codecs.open(filename, 'rb')
    reader = csv.DictReader(data_in)
    for line in reader:
        email = line.pop('email')
        result[email] = line

    return result


def create_symbol_dict(context, email, category, first_name_key):

    # Dictionary to return
    result = {}

    # Get the indicators
    indicators = accumulate.models.Indicator.objects.filter(
            user__email=email,
            version=category,
            context=context)

    # Get the personal record
    try:
        user_pr = user_mgmt.models.PersonalRecord.objects.get(
             user__email=email)
        user_pr = json.loads(user_pr.dictionary)
    except user_mgmt.models.PersonalRecord.DoesNotExist:
        print('Severe error. Personal records not found', end=' ', file=sys.stderr)
        print('for email', email, file=sys.stderr)
        sys.exit(1)

    # GET THE FIRST NAME!!
    result['name'] = \
        user_pr.get(first_name_key, None).split(' ')[0].capitalize()
    if result['name'] is None:
        print('No first name found for user ', end=' ', file=sys.stderr)
        print(email, file=sys.stderr)
        sys.exit(1)

    # Loop over all the indicators
    for indicator in indicators:

        itype = indicator.type
        if itype == 'EVC_COL_EXP':
            pass
        elif itype == 'EVC_COL_EXP':
            pass
        elif itype == 'EVC_DURATION':
            pass
        elif itype == 'EVC_RESOURCE_VIEW':
            pass
        elif itype == 'EVC_EXCO':
            prefix = '_'.join([itype, indicator.resource_id, str(category)])
            # Get the payload
            payload = json.loads(indicator.payload)
            ex_stats = payload[1]
            cor = sum([y[0] for x, y in list(ex_stats.items())])
            inc = sum([y[1] for x, y in list(ex_stats.items())])
            result[prefix + '_COR'] = cor
            result[prefix + '_INC'] = inc
        elif itype == 'EV_XY_MAP':
            pass
        elif itype == 'EVC_DBOARDVIEW':
            pass
        elif itype == 'EVC_FORM_SUBMIT':
            pass
        elif itype == 'EVC_PIAZZA':
            pass
        elif itype == 'EVC_VIDEO':
            prefix = '_'.join([itype, indicator.resource_id, str(category)])
            # Get the payload
            payload = json.loads(indicator.payload)
            result[prefix + '_PLAY'] = payload[0]
            result[prefix + '_PAUSE'] = payload[1]
            result[prefix + '_SECS'] = payload[4]
        elif itype == 'CMT_YOURSTRATEGY':
            pass
        elif itype == 'EVC_MCQ':
            # Get the prefix, but accumulate for all questions in section.
            rid = re.sub('_[0-9]+$', '', indicator.resource_id)
            prefix = '_'.join([itype, rid, str(category)])

            # Get the payload
            payload = json.loads(indicator.payload)
            label = prefix + '_COR'
            result[label] = result.get(label, 0) + payload[0]
            label = prefix + '_INC'
            result[label] = result.get(label, 0) + payload[1]
        elif itype == 'SCORE':
            pass
        else:
            print('Indicator type', itype, 'unknown', file=sys.stderr)
            sys.exit(1)

    # Load the context
    # ctx_handler, ctx_object = user_mgmt.load_context_module(context)
    
    # Expand the dictionary now to obtain the context specific elements
    # result = ctx_handler.expand_dictionary_with_scores(result,
    #                                                    user_django_data.id,
    #                                                    category)
    return result


def grouper(n, iterable, fillvalue=None):
    """grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"""
    args = [iter(iterable)] * n
    return [x for x in itertools.zip_longest(*args, fillvalue=fillvalue)]


def run():
    """Script that receives a context name, a version, and a set of files and
    performs the following operations:

    1. Obtains from the D2U database the users that are part of the
       context.

    2. Loops over all the users in the context. For each user it fetches all
       the indicators.

    3. Processes all the files given as parameters line by line. These files
       are a sequence of sections separated by lines with the structure:

       .. exp-eval:: [PYTHON EXPRESSION]

    4. When one of the previous lines is found, the script evaluates the
       [PYTHON EXPRESSION] in the context of the symbols stored in the
       dictionary and returns a True/False.

       - If the result is false, the remaining section of the file (that is,
         all the lines until a new `.. exp-eval:: ... `are ignored.

       - If the result is TRUE, the lines in the section are read and formatted
         according to the given dictionary.

    5. If there is a line that starts with '.. else::' the following lines will
       be included if the expression evaluated last is false.

    6. Lines starting with .. and followed by any other text are comments.

    7. All the lines are accumulated in a string.

    8. Dump the msg in a file


    Examples of how to invoke the script:

    script -d -c elec1601 -u abelardo.pardo@sydney.edu.au -t 1 feedback1.txt
    script -c elec1601 -t 1 feecback*.txt

    where:
    -d turns on debug

    -h shows the help message

    -s [expr] dumps the symbols from the dictionary matching the expression

    -c [context name] is the context name

    -f [first name string key] Key to search for the first name in record

    -u [email] is the user id (may appear multiple times). If none is given,
       all the users in the context are taken.

    -t [version] is the version

    -i email@to.ignore

    -x Run in strict mode. Stop if a file has all its conditions false
       (disabled by default)
    """

    try:
        opts, args = getopt.getopt(sys.argv[1:], "dhxu:c:f:s:t:i:")
    except getopt.GetoptError as e:
        print(e.msg)
        print(run.__doc__)
        sys.exit(2)

    # Default values for the options:
    given_users = []
    given_category = None
    given_context = None
    given_symbol_dump = None
    first_name_key = 'First Name'
    emails_to_ignore = []
    strict_mode = False

    # Process the arguments
    for optstr, value in opts:
        if optstr == "-d":
            data2u.settings.DEBUG = True
        elif optstr == "-h":
            print(run.__doc__)
            sys.exit(0)
        elif optstr == "-u":
            given_users.append(value)
        elif optstr == "-c":
            given_context = value
        elif optstr == "-f":
            first_name_key = value
        elif optstr == "-t":
            given_category = value
        elif optstr == "-i":
            emails_to_ignore.append(value)
        elif optstr == "-s":
            given_symbol_dump = value
        elif optstr == "-x":
            strict_mode = True

    # Check that all parameters are given and are correct
    terminate = False
    if given_context is None:
        print('Option -c [context name] needs to be included')
        terminate = True
    if given_category is None:
        print('Option -t [version name] needs to be included')
        terminate = True
    if len(args) == 0:
        print('You need to include file names as final arguments')
        terminate = True
    f_no_exist = next((f for f in args if not os.path.exists(f)), None)
    if f_no_exist is not None:
        print('File', f_no_exist, 'does not exist.')
        terminate = True
    if terminate:
        sys.exit(1)

    if given_symbol_dump is not None:
        print('Warning: Option -s given, files will be ignored.')

    if not given_users:
        # Load as list of users those that are context members
        given_users = user_mgmt.models.D2User.objects.filter(
            contexts__name=given_context
        )
    else:
        given_users = user_mgmt.models.D2User.objects.filter(
            email__in=given_users, contexts__name=given_context
        )

    # Loop over all the given users
    for user in given_users:

        # Get the email
        user_email = user.email

        # Ignore the emails given as parameters
        if user_email in emails_to_ignore:
            continue

        # Store a trace of the conditions and its valuations
        cond_trace = [user_email]

        # Get the dictionary for the evaluation of the database
        symbols = create_symbol_dict(given_context,
                                     user_email,
                                     given_category,
                                     first_name_key)

        # If user requested to dump the dictionary, do so.
        if given_symbol_dump is not None:
            for (k, v) in sorted([(a, b) for (a, b) in list(symbols.items())
                                  if re.match(given_symbol_dump, a)]):
                if (type(v) is int) or (type(v) is str) \
                        or (type(v) is str) or (type(v) is float):
                    print(k, v)
                else:
                    print(k, django.forms.models.model_to_dict(v))
            continue

        message = ''

        # Varible encoding if a final evaluation is being done
        # (.. exp-final-eval::)
        final_eval = False

        # Loop over the given files
        for f_name in args:
            cond_in_file = []
            # print '    Processing', f_name
            data_in = codecs.open(f_name, 'r')

            # Accumulate here the message derived from this file
            file_message = ''

            # Variable capturing the value of the last eval operation. None
            # means that no condition has yet being evaluated.
            eval_flag = None
            line_count = 0
            condition_used = False  # To see if any condition has been used.
            for line in data_in:
                line_count += 1

                # Match to detect the exp-eval expression
                m_eval = expr_mark_re.match(line)
                m_final_eval = expr_mark_final_re.match(line)

                # If we detect a match, but we just evaluated a final
                # expression and it was true, we have to terminate looping on
                # this file.
                if (m_eval is not None or m_final_eval is not None) and \
                        final_eval:
                    break

                # CASE A: Line with condition
                if m_eval is not None or m_final_eval is not None:
                    if m_final_eval is not None:
                        # This is the case of evaluating an expression in a
                        # final block.
                        m_eval = m_final_eval

                    # Line contains expression proceed to evaluate!

                    try:
                        eval_flag = eval(m_eval.group('exp_str'))
                    except Exception as e:
                        print('Incorrect expression in file', f_name, end=' ')
                        print('line', line_count)
                        print(e)
                        print()
                        # print symbols
                        print(m_eval.group('exp_str'))
                        sys.exit(1)

                        # We assign this to False to ignore text.
                        # eval_flag = False
                        # cond_in_file.append(None)
                    else:
                        # Store the value of the evaluation in the trace
                        cond_in_file.append(eval_flag)

                    # If it is a final evaluation, push eval_flag to final_eval
                    if m_final_eval is not None:
                        final_eval = eval_flag

                    # Bypass the rest of the code, continue with the next line
                    if case_debug:
                        print('CASE A', line)
                    continue

                # CASE B: If the line is an else, flip the condition
                if line.startswith('.. else::'):
                    eval_flag = not eval_flag
                    if case_debug:
                        print('CASE B', line)
                    continue

                # CASE C: If the line starts with '.. ' is a comment bypass
                if line.startswith('.. '):
                    if case_debug:
                        print('CASE C', line)
                    continue

                # CASE D: If eval_flag is None, include the line and keep going
                if eval_flag is None:
                    # Print the formatted line
                    file_message += line.format(**symbols)
                    if case_debug:
                        print('CASE D', line)
                    continue

                # CASE E: If the flag is false, bypass line
                if not eval_flag:
                    if case_debug:
                        print('CASE E', line)
                    continue

                # CASE F: At this point, the line is being printed due to the
                # effect of one of the conditions.
                condition_used = True
                if case_debug:
                    print('CASE F', line)
                file_message += line.format(**symbols)

            # Once finished the file, check if any of the conditions was
            # used. If so, we accumulate the file_message into
            # message. Otherwise, we ignore with a print
            if condition_used:
                message += file_message
            else:
                if strict_mode:
                    print('Empty file', f_name, 'for user', user_email)
                    sys.exit(1)

            # Accumulate the traces in the global result
            cond_trace.append((f_name, cond_in_file))

            data_in.close()

            # If the final condition has been detected to be TRUE, terminate
            if final_eval:
                break

        # Collate all the flag results
        all_results = [item for f, l in cond_trace[1:] for item in l]

        if (None not in all_results) and (True in all_results):
            # Dump the message
            # File name to dump the result
            out_file_name = '{0}_{1}_{2}.html'.format(given_context,
                                                      user_email.replace('.',
                                                                         '_').
                                                      replace('@', '_at_'),
                                                      given_category)
            data_out = codecs.open(out_file_name, 'w')
            data_out.write(message)
            data_out.write('\n')
            data_out.close()
        else:
            print('File not created for', user_email)
            print('Condition trace:')
            print(all_results)
            sys.exit(1)

        # Create a HEX signature for the combination of answers
        if len(all_results) == 1:
            signature = [1]
        else:
            signature = [1 if x else 0 for x in all_results]
        # Groups of four
        signature = grouper(4, signature, 0)
        total = 0
        factor = 0
        for a, b, c, d in signature:
            total += (a * 1 + b * 2 + c * 4 + d * 8) * 16**factor
            factor += 1
        print(user_email, "{0:#0{1}x}".format(total, 32))
        # End of student loop

    # End of function

# Execution as script
if __name__ == "__main__":

    # dictionary =  create_symbol_dict('elec1601', 
    #                                  'ysha7375@uni.sydney.edu.au', 
    #                                  1)
    # # dictionary =  create_symbol_dict('elec1601', 
    # #                                  'abelardo.pardo@sydney.edu.au', 
    # #                                  1)

    # for (k, v) in sorted(dictionary.items()):
    #     print k, django.forms.models.model_to_dict(v)

    run()
