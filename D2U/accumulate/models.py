#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#

from django.conf import settings
from django.db import models

import user_mgmt.models


class FilterFootprint(models.Model):

    # Context identifying the set of variables
    context = models.ForeignKey(user_mgmt.models.Context,
                                on_delete=models.CASCADE)

    # Filter name
    filter_name = models.CharField(max_length=2048, null=False)

    # The last event that was processed
    id_last_event_processed = models.IntegerField(default=-1)

    # The last time the events were processed
    datetime_last_processed = models.DateTimeField(null=True)


class Indicator(models.Model):
    """
    This table stores information about indicators with the following fields:

    - user_id: Pointer to the user_id in the user_mgmt models

    - type: String indicating the type of indicator.

      Current types:

      - EVC_DBOARDVIEW
      - EVC_DOWNLOAD
      - EVC_DURATION
      - EVC_COL_EXP
      - EVC_EXCO
      - EVC_MCQ
      - EVC_RESOURCE_VIEW
      - EVC_VIDEO
      - EVC_VMCQ
      - EVC_WEEK
      - SC_EXCO
      - SC_MCQ
      - SC_VIDEO
      - SC_VMCQ

    - context: String identifying the context from which the event has been
              collected.

    - resource_id: String identifying the resource referred by the indicator
                   (it could be empty)

    - version: integer indicating the version (evolution over time?)

    - updated: datetime of last update

    - payload: JSON string containing a dictionary

      Current payload structures:

      - EVC_DBOARDVIEW: #hits (activity_id is the WEEK)

      - EVC_DOWNLOAD: #hits (activity_id is the file)

      - EVC_DURATION: [#A, #B, #C, #D, #E, #F, #G]

      - EVC_COL_EXP: [#exp, #col]

      - EVC_EXCO: [score, {exercise: [#cor, #inc]}]

      - EVC_MCQ: [#cor, #inc, #show]

      - EVC_RESOURCE_VIEW: #hits

      - EVC_VIDEO: [#play, #pause, #load, #end, #secs, #lastplay]

      - SCORE: Number ('VIDEO', 'VMCQ', 'MCQ' or 'EXCO' is in the activity_id)

    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             db_index=True,
                             blank=True,
                             null=True,
                             on_delete=models.CASCADE)

    context = models.ForeignKey(user_mgmt.models.Context,
                                on_delete=models.CASCADE)

    type = models.CharField(max_length=2048,
                            db_index=True)

    resource_id = models.CharField(max_length=2048)

    version = models.IntegerField(default=-1,
                                  db_index=True)

    updated = models.DateTimeField(blank=True,
                                   null=True)

    payload = models.TextField(default='')
