#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2017 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#
from __future__ import print_function
import sys
import json

import accumulate
import accumulate.models
import accumulate.iepf

from django.utils import timezone
from user_mgmt.models import D2User, Context, PersonalRecord


##############################################################################
#
# Execution of the rules by for a given user
#
###############################################################################
def execute_rules_for_user(rules, msgs, user_indicators, user_profile,
                           ctx_handler, options):
    """

    :param rules: List of rule names to execute
    :param msgs: Array of messages available
    :param week: Week indicator (some rules use it)
    :param user_indicators: List of indicators stored for this user
    :param user_profile: A dictionary with additional user records
    :param ctx_handler: Reference to the class handling this context
    :param options: A dictionary including options for the filter
    :return: List of rules that have been executed

    The function iterates over the set of given rules, evaluates them with
    respect to the given dictionaries, and reflects the changes in the msgs
    directory.
    """
    # Rule count for performance measure
    rule_count = 0

    # Get the debug option from the dictionary
    debug = options.get('debug', False)

    for (name, condition, msg, url) in rules:

        # Skip templates
        if name == '':
            continue

        rule_count += 1

        # If the condition evaluates to true, add the message
        result = eval(condition, {},
                      {'user_indicators': user_indicators,
                       'user_profile': user_profile,
                       'ctx_handler': ctx_handler})
        if result:
            if debug:
                print(name, 'True')
            if url:
                msgs.append('<a href="{0}">{1}</a>'.format(url, msg))
            else:
                msgs.append(msg)
        else:
            if debug:
                print(name, 'False')

    return rule_count


def update(ctx_object, ctx_handler, last_execution, options):
    """

    :param ctx_object: Module containing the context handler
    :param ctx_handler: Reference to the handler class inside the ctx_object
    :param last_execution: Last time this filter has been executed
    :param options: Dictionary with options given to the filter
    :return:
    """

    # Used to measure the throughput (events/sec)
    start_time = timezone.now()

    # Count the number of rules executed
    rule_count = 0

    # Type of indicator to be created
    indicator_type = 'CMT_RULEMSGS'

    # Get the debug option from the dictionary
    debug = options.get('debug', False)

    # Get the list of rules. There should be a name in the options attached
    # to the key 'rules'. Otherwise, use the ctx_handler.
    rule_symbol = options.get('rules', None)
    if not rule_symbol:
        rules = ctx_handler.rules
    else:
        rules = eval(rule_symbol)

    # Get the users to execute
    user_list = Context.objects.filter(
        name=ctx_object.name).values_list('members', flat=True)

    # Get the value of the version first from the options, and if not
    # available from the context object
    version = options.get('version', None)
    if version is None:
        version = ctx_handler.get_version()
    else:
        version = 0

    if debug:
        print('Processing rules for', len(user_list),
              'users, version', version)

    # Loop over the users
    for user_id in user_list:
        if debug:
            print('Eval user', user_id)

        # Get the user instance
        user = D2User.objects.get(id=user_id)

        # Get the indicators for the user
        user_indicators = accumulate.models.Indicator.objects.filter(
            user=user
        )

        # See how many of them have been updated after the last execution of
        # the filter, If none, there is no need to update this user
        if last_execution.datetime_last_processed:
            new_indicators = user_indicators.filter(
                updated__gte=last_execution.datetime_last_processed)
        else:
            new_indicators = user_indicators

        if len(new_indicators) == 0:
            # No need to process this user as no indicator has been updated
            # since the rules were evaluated.
            continue

        # Transform the indicators into a dictionary
        ind_dict = dict([(x.type + "__" + x.resource_id + "__"
                          + str(x.version),
                          json.loads(x.payload))
                         for x in user_indicators])

        # Get the personal record for the user and translate it into a dict
        user_pr = PersonalRecord.objects.get(user=user)
        user_profile = json.loads(user_pr.dictionary)

        # Initial content of the messages for the user
        msgs = []

        rule_count += execute_rules_for_user(rules,
                                             msgs,
                                             ind_dict,
                                             user_profile,
                                             ctx_handler,
                                             options)

        # Get the rule msg indicator for the current user
        rulemsgs = accumulate.iepf.get_indicator(user,
                                                 indicator_type,
                                                 ctx_object,
                                                 '',  # No
                                                 version)

        # Get the payload of the CMT_RULEMSGS indicator to modify it and
        # update its content
        user_msg = json.loads(rulemsgs.payload)
        user_msg['msgs'] = msgs
        rulemsgs.payload = json.dumps(user_msg)
        rulemsgs.save()

    # Elapsed time
    elapsed_time = timezone.now() - start_time
    print('Speed:', 1.0 * rule_count / elapsed_time.seconds, 'rules/sec',
          file=sys.stderr)

    # Filter has executed properly, update now the time of last execution.
    last_execution.datetime_last_processed = start_time
    last_execution.save()
