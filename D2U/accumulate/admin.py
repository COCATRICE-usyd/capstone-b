#!/usr/bin/env python
# -*- coding: UTF-8 -*-#
#
# Copyright (C) 2014 The University of Sydney
# This file is part of the data2u toolkit

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor
# Boston, MA  02110-1301, USA.
#
# Author: Abelardo Pardo (abelardo.pardo@sydney.edu.au)
#

from django.contrib import admin

import accumulate.models


class FilterFootprintAdmin(admin.ModelAdmin):
    list_display = ('context', 'filter_name', 'id_last_event_processed',
                    'datetime_last_processed')

    search_fields = ['context__name', 'filter_name', 'id_last_event_processed',
                     'datetime_last_processed']


class IndicatorAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'context', 'resource_id', 'version',
                    'updated', 'payload')
    search_fields = ['user', 'type', 'context', 'resource_id', 'version',
                     'updated', 'payload']

admin.site.register(accumulate.models.FilterFootprint,
                    FilterFootprintAdmin)
admin.site.register(accumulate.models.Indicator,
                    IndicatorAdmin)
