# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import status

from GoalSettingCenter.models import Students, Teachers, Task, Lecture, Laboratory, Course, StudyKitLab, \
    StudyKitLecture, LabRecord, LectureRecord


def home(request):
    if request.method == 'POST' and 'login' in request.POST:

        username = request.POST['username']
        password = request.POST['password']
        print (username)
        print (password)
        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:

            login(request, user)
            request.session["user_id"] = user.id
            return HttpResponseRedirect('/dashboard')

        else:

            return HttpResponse("login fail", status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'POST' and 'register' in request.POST:
        user = User(
            username=request.POST['username_r'],
            first_name=request.POST['first_name'],
            last_name=request.POST['last_name'],
            email=request.POST['email']
        )
        user.set_password(request.POST['password_r'])
        user.save()

        student = Students(
            user=user,
            Faculty=request.POST['faculty'],
            Degree=request.POST['degree']
        )
        student.save()

    return render(request, "home.html")


def dashboard(request):
    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)
    try:

        student_object = Students.objects.get(user=user_object)

    except Students.DoesNotExist:

        student_object = None

    try:

        teacher_object = Teachers.objects.get(user=user_object)

    except Teachers.DoesNotExist:

        teacher_object = None

    if request.method == 'POST' and 'newtask' in request.POST:
        task_object = Task(
            owner=user_object,
            Name=request.POST['name'],
            Description=request.POST['description'],
            Additional=request.POST['additional'],
            Priority=request.POST['priority'],
            Duration=request.POST['duration'],
            Date_Due=request.POST['duedate']

        )
        task_object.save()
    if request.method == 'POST' and 'star_task' in request.POST:
        star_the_task = Task.objects.get(id=request.POST['star_task'])
        star_the_task.Star = True
        star_the_task.save()

    if request.method == 'POST' and 'unstar_task' in request.POST:
        open_task = Task.objects.get(id=request.POST['unstar_task'])
        open_task.Star = False
        open_task.save()

    if request.method == 'POST' and 'done_task' in request.POST:
        done_task = Task.objects.get(id=request.POST['done_task'])
        done_task.Status = "Past"
        done_task.save()

    if request.method == 'POST' and 'open_task' in request.POST:
        open_task = Task.objects.get(id=request.POST['open_task'])
        open_task.Status = "Open"
        open_task.save()

    if request.method == 'POST' and 'delete_task' in request.POST:
        delete_task = Task.objects.get(id=request.POST['delete_task'])
        delete_task.delete()

    Past_Tasks = Task.objects.filter(owner=user_object, Status="Past")
    Todo_Tasks = Task.objects.filter(owner=user_object, Status="Open")
    Star_Tasks = Task.objects.filter(owner=user_object, Star=True)
    Tasks = Task.objects.filter(owner=user_object)
    courses = Course.objects.all()
    view = "goal"
    context = {
        "Past_Tasks": Past_Tasks,
        "Todo_Tasks": Todo_Tasks,
        "Star_Tasks": Star_Tasks,
        "user": user_object,
        "courses": courses,
        "Tasks": Tasks,
        "view": view
    }
    return render(request, "dashboard.html", context)


def studykit(request):
    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)
    try:

        student_object = Students.objects.get(user=user_object)

    except Students.DoesNotExist:

        student_object = None

    try:

        teacher_object = Teachers.objects.get(user=user_object)

    except Teachers.DoesNotExist:

        teacher_object = None

    courses = Course.objects.all()
    lab_kits = StudyKitLab.objects.filter(student=student_object)
    lec_kits = StudyKitLecture.objects.filter(student=student_object)

    if request.method == 'POST' and 'delete_lab' in request.POST:
        delete_lab_kit = StudyKitLab.objects.get(id=request.POST["delete_lab"])
        delete_lab_kit.delete()

    if request.method == 'POST' and 'delete_lecture' in request.POST:
        delete_lecture_kit = StudyKitLecture.objects.get(id=request.POST["delete_lecture"])
        delete_lecture_kit.delete()

    view = "studykit"
    context = {

        "lab_kits": lab_kits,
        "lec_kits": lec_kits,
        "courses": courses,
        "lec_kit_count": lec_kits.count(),
        "lab_lit_count": lab_kits.count(),
        "view": view
    }
    return render(request, "StudyKit.html", context)


def lec(request, id):
    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)

    try:

        student_object = Students.objects.get(user=user_object)

    except Students.DoesNotExist:

        student_object = None

    try:

        teacher_object = Teachers.objects.get(user=user_object)

    except Teachers.DoesNotExist:

        teacher_object = None

    lecture_current = Lecture.objects.get(pk=id)
    courses = Course.objects.all()
    lec_record = LectureRecord()
    if request.method == 'POST' and 'addlec' in request.POST:
        lec_kits = StudyKitLecture.objects.filter(student=Students.objects.get(user=user_object))
        if len(lec_kits) == 0:
            lec_kit = StudyKitLecture(
                student=Students.objects.get(user=user_object),
                lecture=lecture_current
            )
            lec_kit.save()
            return HttpResponseRedirect('/dashboard/studykit')
        else:
            for kit in lec_kits:
                if kit.student != Students.objects.get(user=user_object) or kit.lecture != lecture_current:
                    lec_kit = StudyKitLecture(
                        student=Students.objects.get(user=user_object),
                        lecture=lecture_current
                    )
                    lec_kit.save()
                    return HttpResponseRedirect('/dashboard/studykit')
    view = "course"
    context = {

        "lecture": lecture_current,
        "courses": courses,
        "view": view
    }
    return render(request, "Lecture.html", context)


def lab(request, id):
    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)
    lab_current = Laboratory.objects.get(pk=id)
    courses = Course.objects.all()
    if request.method == 'POST' and 'addlab' in request.POST:
        print("post")
        lab_kit = StudyKitLab(
            student=Students.objects.get(user=user_object),
            lab=lab_current
        )
        lab_kit.save()
    view = "course"
    context = {

        "lab": lab_current,
        "courses": courses,
        "view": view
    }
    return render(request, "Laboratory.html", context)


def calendar(request):

    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)
    try:

        student_object = Students.objects.get(user=user_object)

    except Students.DoesNotExist:

        student_object = None

    try:

        teacher_object = Teachers.objects.get(user=user_object)

    except Teachers.DoesNotExist:

        teacher_object = None

    open_tasks = Task.objects.filter(owner=user_object, Status="Open", Star=False)
    close_tasks = Task.objects.filter(owner=user_object, Status="Past", Star=False)
    star_tasks = Task.objects.filter(owner=user_object, Status="Open", Star=True)

    view = "calendar"
    context = {

        "open_tasks": open_tasks,
        "star_tasks": star_tasks,
        "closes_tasks": close_tasks,
        "view": view
    }
    return render(request, "Calendar.html", context)


def analytics(request):

    sender_id = request.session['user_id']
    user_object = User.objects.get(pk=sender_id)
    try:

        student_object = Students.objects.get(user=user_object)

    except Students.DoesNotExist:

        student_object = None

    try:

        teacher_object = Teachers.objects.get(user=user_object)

    except Teachers.DoesNotExist:

        teacher_object = None

    view = "analytics"

    context = {

        "view": view
    }
    return render(request, "Analytics.html", context)