# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from GoalSettingCenter.models import *

# Register your models here.


class LectureModelAdmin(admin.ModelAdmin):
    list_display = ["instructor"]

    class Meta:
        model = Lecture


admin.site.register(Lecture, LectureModelAdmin)


class LabModelAdmin(admin.ModelAdmin):
    list_display = ["tutor"]

    class Meta:
        model = Laboratory


admin.site.register(Laboratory, LabModelAdmin)


class CourseModelAdmin(admin.ModelAdmin):
    list_display = ["lecture", "lab"]

    class Meta:
        model = Course


admin.site.register(Course, CourseModelAdmin)


class TeacherModelAdmin(admin.ModelAdmin):
    list_display = ["user"]

    class Meta:
        model = Teachers


admin.site.register(Teachers, TeacherModelAdmin)


class StudentModelAdmin(admin.ModelAdmin):
    list_display = ["user"]

    class Meta:
        model = Students


admin.site.register(Students, StudentModelAdmin)


class TaskModelAdmin(admin.ModelAdmin):
    list_display = ["owner"]

    class Meta:
        model = Task


admin.site.register(Task, TaskModelAdmin)