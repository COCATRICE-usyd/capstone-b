# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from django.contrib.auth.models import User
from django.db import models
# Create your models here.


class Teachers(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,

    )
    TeacherID = models.CharField(max_length=20)
    Department = models.CharField(max_length=20)


class Students(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,

    )
    UniKey = models.CharField(max_length=20)
    Degree = models.CharField(max_length=20)
    Faculty = models.CharField(max_length=30)


class Task(models.Model):
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='creator'
    )
    Name = models.CharField(max_length=30)
    Description = models.CharField(max_length=300)
    Additional = models.CharField(max_length=200)
    Priority = models.CharField(max_length=50)
    Date_Created = models.DateField(auto_now=False, auto_now_add=True)
    Duration = models.IntegerField()
    Date_Due = models.DateField(default="2000-01-01")
    Status = models.CharField(max_length=50, default="Open")
    Star = models.BooleanField(default=False)


class Lecture(models.Model):

    instructor = models.ForeignKey(
        Teachers,
        on_delete=models.CASCADE,
        related_name='instructor'
    )
    Date_Created = models.DateField(auto_now=False, auto_now_add=True)
    Content = models.TextField()
    Name = models.CharField(max_length=200, default="Lecture")


class Laboratory(models.Model):

    tutor = models.ForeignKey(
        Teachers,
        on_delete=models.CASCADE,
        related_name='tutor'
    )
    Date_Created = models.DateField(auto_now=False, auto_now_add=True)
    Content = models.TextField()
    Name = models.CharField(max_length=200, default="Lab")


class Course(models.Model):

    lab = models.ForeignKey(
        Laboratory,
        on_delete=models.CASCADE,
        related_name='lab'
    )
    lecture = models.ForeignKey(
        Lecture,
        on_delete=models.CASCADE,
        related_name='lecture'
    )
    WeekLabel = models.CharField(max_length=20)


class StudyKitLab(models.Model):
    student = models.ForeignKey(
        Students,
        on_delete=models.CASCADE,
        related_name='student_kit_lab'
    )
    lab = models.ForeignKey(
        Laboratory,
        on_delete=models.CASCADE,
        related_name='lab_kit'
    )
    Date_Created = models.DateField(auto_now=False, auto_now_add=True)


class StudyKitLecture(models.Model):
    student = models.ForeignKey(
        Students,
        on_delete=models.CASCADE,
        related_name='student_kit_lecture'
    )
    lecture = models.ForeignKey(
        Lecture,
        on_delete=models.CASCADE,
        related_name='lecture_kit'
    )
    Date_Created = models.DateField(auto_now=False, auto_now_add=True)


class LectureRecord(models.Model):

    student = models.ForeignKey(
        Students,
        on_delete=models.CASCADE,
        related_name='student_view_lecture'
    )

    lecture = models.ForeignKey(
        Lecture,
        on_delete=models.CASCADE,
        related_name='lecture_record'
    )

    count = models.IntegerField(default=0)
    record_date = models.DateField()


class LabRecord(models.Model):

    student = models.ForeignKey(
        Students,
        on_delete=models.CASCADE,
        related_name='student_view_lab'
    )

    lab = models.ForeignKey(
        Laboratory,
        on_delete=models.CASCADE,
        related_name='lab_record'
    )

    count = models.IntegerField(default=0)
    record_date = models.DateField()
