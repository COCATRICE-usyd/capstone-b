from django.conf.urls import url
from django.contrib import admin

from GoalSettingCenter import views

urlpatterns = [

    url(r'^$', views.dashboard, name="dashboard"),
    url(r'^studykit/', views.studykit, name='studykit'),
    url(r'^lab/(?P<id>\d+)/$', views.lab, name='lab'),
    url(r'^lecture/(?P<id>\d+)/$', views.lec, name='lecture'),
    url(r'^calendar/', views.calendar, name='calendar'),
    url(r'^analytics/', views.analytics, name='analytics')
]
